import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.io.*;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.ResourceBundle;

public class EmulatorController implements Initializable
{
    private Emulator emulator;

    @FXML private Button btnStart;
    @FXML private Button btnStop;

    @FXML private Button btnHostsAdd;
    @FXML private Button btnHostsRemove;

    @FXML private Text lblInfo;


    public void initialize(URL location, ResourceBundle resources)
    {
        emulator = new Emulator();
        emulator.init();
    }

    @FXML
    protected void onStart(ActionEvent event)
    {
        boolean hostsValid = false;
        try
        {
            hostsValid = hasEntriesInHosts();
        } catch (IOException e)
        {
            e.printStackTrace();
        }

        if(!hostsValid)
        {
            Alert alert = new Alert(Alert.AlertType.ERROR);
            alert.setTitle("Error!");
            alert.setHeaderText("NOO, THIS CANNOT BE!");
            alert.setContentText("Please click on 'Edit Hosts' button to insert entries into the hosts before starting the server");
            alert.showAndWait();
            return;
        }

        lblInfo.setText("Server Started!");
        emulator.start();

        btnStart.setDisable(true);
        btnStop.setDisable(false);

        btnHostsAdd.setDisable(true);
        btnHostsRemove.setDisable(true);
    }

    @FXML
    protected void onStop(ActionEvent event)
    {
        lblInfo.setText("Server Stopped!");
        emulator.stop();

        btnStart.setDisable(false);
        btnStop.setDisable(true);

        btnHostsAdd.setDisable(false);
        btnHostsRemove.setDisable(false);
    }

    @FXML
    protected void onHostsAdd(ActionEvent event)
    {
        boolean addedKB = false;
        boolean addedAUTH = false;

        boolean addedKBv6 = false;
        boolean addedAUTHv6 = false;
        try
        {
            List<String> hostsContent = readFileContent("C:\\Windows\\System32\\drivers\\etc\\hosts");

            if(!hostsContent.contains(Emulator.HOSTS_ENTRY_KB))
            {
                hostsContent.add(Emulator.HOSTS_ENTRY_KB);
                addedKB = true;
            }
            if(!hostsContent.contains(Emulator.HOSTS_ENTRY_AUTH))
            {
                hostsContent.add(Emulator.HOSTS_ENTRY_AUTH);
                addedAUTH = true;
            }

            if(!hostsContent.contains(Emulator.HOSTS_ENTRY_KB_IP6))
            {
                hostsContent.add(Emulator.HOSTS_ENTRY_KB_IP6);
                addedKBv6 = true;
            }
            if(!hostsContent.contains(Emulator.HOSTS_ENTRY_AUTH_IP6))
            {
                hostsContent.add(Emulator.HOSTS_ENTRY_AUTH_IP6);
                addedAUTHv6 = true;
            }

            if(addedKB || addedAUTH|| addedKBv6 || addedAUTHv6)
                writeToFile(hostsContent, "C:\\Windows\\System32\\drivers\\etc\\hosts");

        } catch (IOException e)
        {
            e.printStackTrace();
        }

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Info!");
        alert.setHeaderText("HOSTS File:");

        if(!addedKB && !addedAUTH)
            alert.setContentText("No change has been made, entries are already present!");
        else
            alert.setContentText("Correctly edited the hosts, you can now start the server!");

        alert.showAndWait();

    }

    @FXML
    protected void onHostsRemove(ActionEvent event)
    {
        boolean removedKB = false;
        boolean removedAUTH = false;

        boolean removedKBv6 = false;
        boolean removedAUTHv6 = false;
        try
        {
            List<String> hostsContent = readFileContent("C:\\Windows\\System32\\drivers\\etc\\hosts");

            if(hostsContent.contains(Emulator.HOSTS_ENTRY_KB))
            {
                hostsContent.remove(Emulator.HOSTS_ENTRY_KB);
                removedKB = true;
            }
            if(hostsContent.contains(Emulator.HOSTS_ENTRY_AUTH))
            {
                hostsContent.remove(Emulator.HOSTS_ENTRY_AUTH);
                removedAUTH = true;
            }

            if(hostsContent.contains(Emulator.HOSTS_ENTRY_KB_IP6))
            {
                hostsContent.remove(Emulator.HOSTS_ENTRY_KB_IP6);
                removedKBv6 = true;
            }
            if(hostsContent.contains(Emulator.HOSTS_ENTRY_AUTH_IP6))
            {
                hostsContent.remove(Emulator.HOSTS_ENTRY_AUTH_IP6);
                removedAUTHv6 = true;
            }

            if(removedKB || removedAUTH || removedKBv6 || removedAUTHv6)
                writeToFile(hostsContent, "C:\\Windows\\System32\\drivers\\etc\\hosts");

        } catch (IOException e)
        {
            e.printStackTrace();
        }

        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Info!");
        alert.setHeaderText("HOSTS File:");

        if(!removedKB && !removedAUTH)
            alert.setContentText("No change has been made, hosts is clean!");
        else
            alert.setContentText("Hosts has been cleaned!");

        alert.showAndWait();
    }

    private boolean hasEntriesInHosts() throws IOException
    {
        List<String> hostsContent = readFileContent("C:\\Windows\\System32\\drivers\\etc\\hosts");

        return hostsContent.contains(Emulator.HOSTS_ENTRY_KB) && hostsContent.contains(Emulator.HOSTS_ENTRY_AUTH)
                && hostsContent.contains(Emulator.HOSTS_ENTRY_KB_IP6) && hostsContent.contains(Emulator.HOSTS_ENTRY_AUTH_IP6);
    }

    private List<String> readFileContent(String filePath) throws IOException
    {
        Charset charset = Charset.defaultCharset();

        Path path = Paths.get(filePath);

        return Files.readAllLines(path, charset);
    }

    private void writeToFile(List<String> content, String filePath) throws IOException
    {
        FileWriter fw = new FileWriter(filePath);

        for (int i = 0; i < content.size(); i++)
        {
            fw.write(content.get(i) + "\n");
        }
        fw.flush();
        fw.close();
    }

    private void test()
    {

    }
}
