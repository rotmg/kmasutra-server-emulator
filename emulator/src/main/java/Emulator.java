import com.sun.net.httpserver.HttpContext;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class Emulator
{
    public static void main(String[] args)
    {
        Emulator emulator = new Emulator();
        emulator.init();
        emulator.start();
    }

    public static final String HOSTS_ENTRY_KB = "127.0.0.1 kronkboxer.cloudapp.net";
    public static final String HOSTS_ENTRY_AUTH = "127.0.0.1 auth.kb.kronks.me";

    public static final String HOSTS_ENTRY_KB_IP6 = "::1 kronkboxer.cloudapp.net";
    public static final String HOSTS_ENTRY_AUTH_IP6 = "::1 auth.kb.kronks.me";


    private HttpServer httpServer = null;
    private HttpServer appServer = null;

    public void init()
    {
        try
        {
            httpServer = HttpServer.create(new InetSocketAddress(80), 0);
            httpServer.createContext("/000kbversion.txt", new VersionEmulator());
            httpServer.setExecutor(null);

            appServer = HttpServer.create(new InetSocketAddress(1227), 0);
            appServer.createContext("/createinstance/", new LoginEmulator());
            appServer.createContext("/slf/", new LoginSlfEmulator());
            appServer.createContext("/pmf/", new LoginPmfEmulator());
            appServer.createContext("/uilf/", new LoginUilfEmulator());
            appServer.createContext("/keepalive/", new KeepAliveEmulator());
            appServer.setExecutor(null);
        } catch (IOException e)
        {
            e.printStackTrace();
        }
    }

    static class TestEmulator implements HttpHandler
    {
        public void handle(HttpExchange httpExchange) throws IOException
        {
            System.out.println("REQUEST: Test");

            URI request = httpExchange.getRequestURI();

            System.out.println("\t"+request);

            String response = "test";
            httpExchange.sendResponseHeaders(200, response.length());
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    static class VersionEmulator implements HttpHandler
    {
        /*
            6
            v1.4.1
            0

        */
        public void handle(HttpExchange httpExchange) throws IOException
        {
            System.out.println("REQUEST: Version");

            String response = "6\nv1.4.1\n0\n ";
            httpExchange.sendResponseHeaders(200, response.length());
            OutputStream os = httpExchange.getResponseBody();
            os.write(response.getBytes());
            os.close();
        }
    }

    static class LoginEmulator implements HttpHandler
    {
        public void handle(HttpExchange httpExchange) throws IOException
        {
            System.out.println("REQUEST: Login");

            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource("login").getFile());
            byte[] content = readFile(file);

            httpExchange.sendResponseHeaders(200, content.length);
            OutputStream os = httpExchange.getResponseBody();
            os.write(content);
            os.close();
        }
    }

    static class LoginSlfEmulator implements HttpHandler
    {
        public void handle(HttpExchange httpExchange) throws IOException
        {
            System.out.println("REQUEST: Login Slf");

            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource("slf").getFile());
            byte[] content = readFile(file);

            httpExchange.sendResponseHeaders(200, content.length);
            OutputStream os = httpExchange.getResponseBody();
            os.write(content);
            os.close();
        }
    }

    static class LoginPmfEmulator implements HttpHandler
    {
        public void handle(HttpExchange httpExchange) throws IOException
        {
            System.out.println("REQUEST: Login Pmf");

            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource("pmf").getFile());
            byte[] content = readFile(file);

            httpExchange.sendResponseHeaders(200, content.length);
            OutputStream os = httpExchange.getResponseBody();
            os.write(content);
            os.close();
        }
    }

    static class LoginUilfEmulator implements HttpHandler
    {
        public void handle(HttpExchange httpExchange) throws IOException
        {
            System.out.println("REQUEST: Login Uilf");

            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource("uilf").getFile());
            byte[] content = readFile(file);

            httpExchange.sendResponseHeaders(200, content.length);
            OutputStream os = httpExchange.getResponseBody();
            os.write(content);
            os.close();
        }
    }

    static class KeepAliveEmulator implements HttpHandler
    {
        public void handle(HttpExchange httpExchange) throws IOException
        {
            System.out.println("REQUEST: KeepAlive");

            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource("keepalive").getFile());
            byte[] content = readFile(file);

            httpExchange.sendResponseHeaders(200, content.length);
            OutputStream os = httpExchange.getResponseBody();
            os.write(content);
            os.close();
        }
    }

    private static byte[] readFile(File file)
    {
        FileInputStream fileInputStream = null;
        byte[] bFile = new byte[(int) file.length()];
        try
        {
            //convert file into array of bytes
            fileInputStream = new FileInputStream(file);
            fileInputStream.read(bFile);
            fileInputStream.close();
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return bFile;
    }

    public void start()
    {
        httpServer.start();
        appServer.start();
    }

    public void stop()
    {
        httpServer.stop(0);
        appServer.stop(0);
    }
}